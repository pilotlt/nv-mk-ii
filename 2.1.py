'''Sfw V2.1 fo NV MkII by Boris Vinikov'''

from datetime import datetime
from gpiozero import Button
from gpiozero import LED
from picamera import PiCamera
from subprocess import check_call
import os

date = datetime.now()
date = date.strftime("%Y_%m_%d_%H_%M_%S")

os.makedirs(r'/media/pi/88E4-B867/{}'.format(date))  # each time RPi is switched on, new folder is created. Current date is used to name the folder

left_button = Button(16) #push button switch
right_button = Button(20) #push button "Record"

camera = PiCamera()
red = LED(21)
red.off()

def shutdown():
    ''' this function describes shutdown process for RPi'''
    check_call(['sudo', 'poweroff'])
    
shutdown_btn = Button(15, hold_time=3)  # "Shutdown" push button, time to hold for shutdown - 3 sec.
shutdown_btn.when_held = shutdown # shutdown RPi on button press

camera.start_preview()

i = 1
while True:
    left_button.when_pressed = camera.start_preview
    left_button.when_released = camera.stop_preview
    right_button.wait_for_press()
    camera.start_recording('/media/pi/88E4-B867/{}/{}.h264'.format(date, i))
    i+=1
    red.on()
    camera.wait_recording(90)
    camera.stop_recording()
    red.off()
   



    
